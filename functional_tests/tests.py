from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(LiveServerTestCase):

    # เปิดเว็บ browser firefox
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    # ปิดเว็บ browser
    def tearDown(self):
        self.browser.quit()

    # check food name in row
    #def check_for_row_in_food_table(self, row_text):
        #table = self.browser.find_element_by_id('id_food_table')
       # rows = table.find_elements_by_tag_name('tr')
        #self.assertIn(row_text, [row.text for row in rows])

    # check comment in row
    #def check_for_row_in_comment_table(self, row_text):
       # table = self.browser.find_element_by_id('id_comment_table')
       # rows = table.find_elements_by_tag_name('tr')
       # self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_cooking_diary_and_retrieve_it_later(self):
        # to check out its homepage
        self.browser.get(self.live_server_url)
        #self.browser.get('http://localhost:8000')
        # ตรวจสอบ title == Cooking_Diary ?
        self.assertIn('Cooking_Diary', self.browser.title)
        # ตรวจสอบ h1 == Wellcome to Cooking Diary ?
        #header_text = self.browser.find_element_by_tag_name('h1').text
        #self.assertIn('Wellcome to Cooking Diary', header_text)
        #ใส่ชื่อร้านอาหาร
        inputbox_restaurant = self.browser.find_element_by_id('restaurant_name')
        inputbox_restaurant.send_keys('Ice cook')

        # หาช่องใส่ชื่อ อาหาร และ ใส่ชื่ออาหาร noodle แล้วกด enter
        inputbox_food = self.browser.find_element_by_id('food_name')
       # self.assertEqual(
       #         inputbox_food.get_attribute('placeholder'),
       #        'Please fill  in food name'
       # )
        inputbox_food.send_keys('noodle')
	#ใส่ พลังงาน
        inputbox_energy = self.browser.find_element_by_id('calorie')
        inputbox_energy.send_keys('200')

        #self.check_for_row_in_food_table('noodle')
        #table = self.browser.find_element_by_id('id_food_table')

        # ใส่ comment
        inputbox_comment = self.browser.find_element_by_id('comment')
        inputbox_comment.send_keys('อาหารอร่อยมาก')
        #inputbox_comment.send_keys(Keys.ENTER)
        inputbox_food.send_keys(Keys.ENTER)

        import time
        time.sleep(5)
        #self.check_for_row_in_comment_table('อาหารอร่อยมาก')
        #table = self.browser.find_element_by_id('id_comment_table')
        #import time
        #time.sleep(5)

        self.fail('Finish the test!')
