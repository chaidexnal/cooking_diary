from django.db import models

class Food(models.Model):
    restaurant = models.TextField(default='')
    foodname = models.TextField(default='')
    commentfood = models.TextField(default='')
    calorie = models.TextField(default='')
