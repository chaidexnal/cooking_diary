from django.shortcuts import redirect, render
from cooking_diary.models import Food

def home_page(request):
    if request.method == 'POST':
        #new_food_name = request.POST['food_names']
        Food.objects.create(foodname=request.POST['food_names'],
                            commentfood=request.POST.get('comments',''),
                            calorie=request.POST.get('calories',''),
                            restaurant=request.POST.get('restaurant_names','')
                           )
        return redirect('/')

    foods= Food.objects.all()
    #comments= Food.objects.all()
    #calories= Food
    return render(request, 'home.html', {
                  'foods': foods
                  #'comments': comments,
                  #'calories': calories,
                  #'restaurants': restaurants
                  })
    #return render(request, 'home.html', {
        # ชื่ออาหาร
        #'new_food_name': new_food_name,
        #'new_food_name': request.POST.get('food_names', ''),
        # comment
        #'new_comment': request.POST.get('comments', ''),
    #})


