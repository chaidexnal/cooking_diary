from django.core.urlresolvers import resolve
from django.template.loader import render_to_string
from django.test import TestCase
from cooking_diary.views import home_page
from django.http import HttpRequest
from cooking_diary.models import Food


class HomePageTest(TestCase):

    # url home_page
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    # ตรวจสอบ html ที่ return มา
    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode(), expected_html)

    # สร้าง object key = POST, value = food_names หรือ comments
    def test_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['food_names'] = 'A new food name'
        request.POST['comments'] = 'A new comment'

        response = home_page(request)

        #self.assertEqual(Food.objects.count(), 1)
        new_food = Food.objects.first()
        self.assertEqual(new_food.foodname, 'A new food name')

        #self.assertEqual(response.status_code, 302)
        #self.assertEqual(response['location'], '/')

        #expected_html = render_to_string(
            #'home.html',
            #{'new_food_foodname': 'A new food name'}
        #)
        #self.assertIn('A new comment', response.content.decode())
        #self.assertIn('A new food name', response.content.decode())

    def test_home_page_redirects_after_POST(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['food_names'] = 'A new food name'
        request.POST['comments'] = 'A new comment'
        response = home_page(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_home_page_only_saves_foods_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Food.objects.count(), 0)


class FoodModelTest(TestCase):

    def test_saving_and_retrieving_foods(self):
        first_food = Food()
        first_food.foodname = 'The first (ever) food name'
        first_food.save()

        #second_food = Food()
        #second_food.foodname = 'food the second'
        #second_food.save()

        saved_foods = Food.objects.all()
        #self.assertEqual(saved_foods.count(), 2)

        first_saved_food = saved_foods[0]
        #second_saved_food = saved_foods[1]
        self.assertEqual(first_saved_food.foodname, 'The first (ever) food name')
        #self.assertEqual(second_saved_food.foodname, 'food the second')



