# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cooking_diary', '0004_auto_20150319_1240'),
    ]

    operations = [
        migrations.AddField(
            model_name='food',
            name='commenttext',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
