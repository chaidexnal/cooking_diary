# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cooking_diary', '0002_item_text'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Item',
            new_name='Food',
        ),
    ]
