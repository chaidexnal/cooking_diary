# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cooking_diary', '0006_remove_food_commenttext'),
    ]

    operations = [
        migrations.AddField(
            model_name='food',
            name='calorie',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='food',
            name='commentfood',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='food',
            name='restaurant',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
